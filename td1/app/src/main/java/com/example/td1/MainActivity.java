package com.example.td1;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;



public class MainActivity extends AppCompatActivity {
    private EditText etGauche;
    private EditText etDroit;
    private TextView tvResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         //setContentView(R.layout.activity_main);
        setContentView(R.layout.activity_secondary);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final Button buttonad = findViewById(R.id.buttonAd);
        buttonad.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                computeAdd(v);

            }
        });
        final Button buttonss = findViewById(R.id.buttonSt);
        buttonss.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                computeSous(v);

            }
        });
        final Button buttonmt = findViewById(R.id.buttonMt);
        buttonmt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                computemulti(v);

            }
        });
        final Button buttondv = findViewById(R.id.buttonDv);
        buttondv.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                computedivi(v);

            }
        });

    }
   public void computeAdd(View v){

            etGauche =  findViewById(R.id.idEditText);
            etDroit = findViewById(R.id.idEditText2);
            tvResult = findViewById(R.id.textView4);
            int hg =0;
            String etGauche2= etGauche.getText().toString();
             String etDroit2= etDroit.getText().toString();
              hg= Integer.parseInt(etGauche2) + Integer.parseInt(etDroit2) ;
              String result = Integer.toString(hg);
        tvResult.setText(result);
    }
    public void computeSous(View v){

        etGauche =  findViewById(R.id.idEditText);
        etDroit = findViewById(R.id.idEditText2);
        tvResult = findViewById(R.id.textView3);
        int hg =0;
        String etGauche2= etGauche.getText().toString();
        String etDroit2= etDroit.getText().toString();
        hg= Integer.parseInt(etGauche2) - Integer.parseInt(etDroit2) ;
        String result = Integer.toString(hg);
        tvResult.setText(result);
    }
    public void computedivi(View v){

        etGauche =  findViewById(R.id.idEditText);
        etDroit = findViewById(R.id.idEditText2);
        tvResult = findViewById(R.id.textView5);
        int hg =0;
        String etGauche2= etGauche.getText().toString();
        String etDroit2= etDroit.getText().toString();
        hg= Integer.parseInt(etGauche2) / Integer.parseInt(etDroit2) ;
        String result = Integer.toString(hg);
        tvResult.setText(result);
    }

    public void computemulti(View v){

        etGauche =  findViewById(R.id.idEditText);
        etDroit = findViewById(R.id.idEditText2);
        tvResult = findViewById(R.id.textView6);
        tvResult = findViewById(R.id.textView6);
        int hg =0;
        String etGauche2= etGauche.getText().toString();
        String etDroit2= etDroit.getText().toString();
        hg= Integer.parseInt(etGauche2) * Integer.parseInt(etDroit2) ;
        String result = Integer.toString(hg);
        tvResult.setText(result);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }





   //partie 2


    }


