package com.example.td33;

import android.graphics.drawable.Drawable;

public class JeuVideo {
    String name;
    Integer price ;
    Drawable img;
    public JeuVideo(String name , Integer price , Drawable img){
        this.name=name;
        this.price=price;
        this.img=img;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public Drawable getImg() {
        return img;
    }

    public void setImg(Drawable img) {
        this.img = img;
    }
}
