package com.example.td33;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MyVideoGamesViewHolder extends RecyclerView.ViewHolder {
    private TextView mNameTV;
    private TextView mPriceTV;
    private ImageView imageView ;
    public MyVideoGamesViewHolder(@NonNull View itemView) {
        super(itemView);
        mNameTV = itemView.findViewById(R.id.name);
        mPriceTV = itemView.findViewById(R.id.price);
        imageView = itemView.findViewById(R.id.imageView);
    }

    void display(JeuVideo jeuVideo) {
        mNameTV.setText(jeuVideo.getName());
        mPriceTV.setText(jeuVideo.getPrice() + "$");
        imageView.setImageDrawable(jeuVideo.getImg());


    }

}