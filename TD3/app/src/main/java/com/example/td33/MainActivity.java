package com.example.td33;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView myRecyclerView = findViewById(R.id.myRecyclerView);
        myRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        //création et ajout de jeu


        ArrayList mesJeux = new ArrayList<JeuVideo>();
        Drawable tekkenImg= getResources().getDrawable(R.drawable.tekken);
        Drawable forniteImg = getResources().getDrawable(R.drawable.fornite);

        JeuVideo tekken = new JeuVideo("tekken", 22,tekkenImg);
        JeuVideo fornite = new JeuVideo("fornite", 2,forniteImg);

        mesJeux.add(tekken);
        mesJeux.add(fornite);


        myRecyclerView.setAdapter(new MyVideoGamesAdapter(mesJeux));

        myRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));

    }


}


