package com.example.td4;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class Repoviewer extends RecyclerView.ViewHolder {
    private TextView nomr;

    public Repoviewer(@NonNull View itemView) {
        super(itemView);
        nomr = itemView.findViewById(R.id.textView);
    }

    void display(RepoList repo) {
        nomr.setText(repo.getName());

    }
}