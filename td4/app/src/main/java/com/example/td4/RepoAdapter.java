package com.example.td4;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class RepoAdapter extends RecyclerView.Adapter<Repoviewer > {


        List<RepoList> repoList;

        public RepoAdapter(List<RepoList> repoList) {


            this.repoList = repoList;
        }
    @NonNull
    @Override
    public Repoviewer onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.repo_view, parent, false);
        return new Repoviewer (view);
    }
    @Override
    public void onBindViewHolder(@NonNull Repoviewer  holder, int position) {
        holder.display(repoList.get(position));
    }
    @Override
    public int getItemCount() {
        return repoList.size();
    }

    }

